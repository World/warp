# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Charles Monzat <charles.monzat@free.fr>, 2022.
# NiniKo <niniko.opensource@larieuse.com>, 2023.
# Irénée THIRION <irenee.thirion@e.email>, 2023.
# Vincent Chatelain <vinchatl_gnome@proton.me>, 2024.
# Julien Humbert <julroy67@gmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: warp\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/warp/issues\n"
"POT-Creation-Date: 2024-10-10 15:35+0000\n"
"PO-Revision-Date: 2024-11-10 11:48+0900\n"
"Last-Translator: Julien Humbert <julroy67@gmail.com>\n"
"Language-Team: French <gnomefr@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Gtranslator 47.0\n"
"X-DL-Lang: fr\n"
"X-DL-Module: warp\n"
"X-DL-Branch: main\n"
"X-DL-Domain: po\n"
"X-DL-State: Translating\n"

#: data/app.drey.Warp.desktop.in.in:3 data/app.drey.Warp.metainfo.xml.in.in:4
#: src/main.rs:133 src/ui/window.ui:81
msgid "Warp"
msgstr "Warp"

#: data/app.drey.Warp.desktop.in.in:4 data/app.drey.Warp.metainfo.xml.in.in:5
#: src/ui/welcome_dialog.ui:24
msgid "Fast and secure file transfer"
msgstr "Transfert de fichier rapide et sécurisé"

#: data/app.drey.Warp.desktop.in.in:9
msgid "Gnome;GTK;Wormhole;Magic-Wormhole;"
msgstr "Gnome;GTK;Wormhole;Magic-Wormhole;Trou de ver;"

#. developer_name tag deprecated with Appstream 1.0
#: data/app.drey.Warp.metainfo.xml.in.in:9 src/ui/window.rs:303
msgid "Fina Wilke"
msgstr "Fina Wilke"

#: data/app.drey.Warp.metainfo.xml.in.in:43
msgid ""
"Warp allows you to securely send files to each other via the internet or "
"local network by exchanging a word-based code."
msgstr ""
"Warp vous permet de transférer des fichiers de manière sécurisée via "
"internet ou un réseau local en échangeant un code basé sur des mots."

#: data/app.drey.Warp.metainfo.xml.in.in:47
msgid ""
"The best transfer method will be determined using the “Magic Wormhole” "
"protocol which includes local network transfer if possible."
msgstr ""
"La meilleure méthode de transfert sera déterminée en utilisant le protocole "
"« Magic Wormhole » qui inclut le transfert par réseau local si possible."

#: data/app.drey.Warp.metainfo.xml.in.in:51
msgid "Features"
msgstr "Fonctionnalités"

#: data/app.drey.Warp.metainfo.xml.in.in:53
msgid "Send files between multiple devices"
msgstr "Envoyer des fichiers entre divers appareils"

#: data/app.drey.Warp.metainfo.xml.in.in:54
msgid "Every file transfer is encrypted"
msgstr "Chaque transfert de fichier est chiffré"

#: data/app.drey.Warp.metainfo.xml.in.in:55
msgid "Directly transfer files on the local network if possible"
msgstr "Transférer directement des fichiers sur le réseau local si possible"

#: data/app.drey.Warp.metainfo.xml.in.in:56
msgid "An internet connection is required"
msgstr "Une connexion internet est requise"

#: data/app.drey.Warp.metainfo.xml.in.in:57
msgid "QR Code support"
msgstr "Prise en charge des codes QR"

#: data/app.drey.Warp.metainfo.xml.in.in:58
msgid ""
"Compatibility with the Magic Wormhole command line client and all other "
"compatible apps"
msgstr ""
"Compatibilité avec le client en ligne de commande « Magic Wormhole » et "
"toutes les autres applications compatibles"

#: data/app.drey.Warp.metainfo.xml.in.in:64
#: data/app.drey.Warp.metainfo.xml.in.in:84
msgid "Main Window"
msgstr "Fenêtre principale"

#. Translators: Entry placeholder; This is a noun
#: data/app.drey.Warp.metainfo.xml.in.in:68
#: data/app.drey.Warp.metainfo.xml.in.in:88 src/ui/window.ui:207
msgid "Transmit Code"
msgstr "Code de transfert"

#: data/app.drey.Warp.metainfo.xml.in.in:72
#: data/app.drey.Warp.metainfo.xml.in.in:92
msgid "Accept File Transfer"
msgstr "Accepter le transfert de fichier"

#: data/app.drey.Warp.metainfo.xml.in.in:76
#: data/app.drey.Warp.metainfo.xml.in.in:96 src/ui/action_view.rs:646
msgid "Receiving File"
msgstr "Réception du fichier"

#: data/app.drey.Warp.metainfo.xml.in.in:80
#: data/app.drey.Warp.metainfo.xml.in.in:100 src/ui/action_view.rs:655
msgid "File Transfer Complete"
msgstr "Transfert du fichier achevé"

#: data/resources/ui/help_overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Général"

#: data/resources/ui/help_overlay.ui:14
msgctxt "shortcut window"
msgid "Show Help"
msgstr "Afficher l’aide"

#: data/resources/ui/help_overlay.ui:20
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Afficher les raccourcis"

#: data/resources/ui/help_overlay.ui:26
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "Afficher les préférences"

#: data/resources/ui/help_overlay.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quitter"

#: data/resources/ui/help_overlay.ui:40
msgctxt "shortcut window"
msgid "File Transfer"
msgstr "Transfert de fichier"

#: data/resources/ui/help_overlay.ui:43
msgctxt "shortcut window"
msgid "Send File"
msgstr "Envoyer un fichier"

#: data/resources/ui/help_overlay.ui:49
msgctxt "shortcut window"
msgid "Send Folder"
msgstr "Envoyer un dossier"

#: data/resources/ui/help_overlay.ui:55
msgctxt "shortcut window"
msgid "Receive File"
msgstr "Recevoir un fichier"

#. Translators: {0} = file size transferred, {1} = total file size, Example: 17.3MB / 20.5MB
#: src/gettext/duration.rs:11
msgctxt "File size transferred"
msgid "{0} / {1}"
msgstr "{0} / {1}"

#. Translators: File transfer time left
#: src/gettext/duration.rs:18
msgid "One second left"
msgid_plural "{} seconds left"
msgstr[0] "Une seconde restante"
msgstr[1] "{} secondes restantes"

#. Translators: File transfer time left
#: src/gettext/duration.rs:26
msgid "One minute left"
msgid_plural "{} minutes left"
msgstr[0] "Une minute restante"
msgstr[1] "{} minutes restantes"

#. Translators: File transfer time left
#: src/gettext/duration.rs:34
msgid "One hour left"
msgid_plural "{} hours left"
msgstr[0] "Une heure restante"
msgstr[1] "{} heures restantes"

#. Translators: File transfer time left
#: src/gettext/duration.rs:42
msgid "One day left"
msgid_plural "{} days left"
msgstr[0] "Un jour restant"
msgstr[1] "{} jours restants"

#. Translators: {0} = 11.3MB / 20.7MB, {1} = 3 seconds left
#: src/gettext/duration.rs:52
msgctxt "Combine bytes progress {0} and time remaining {1}"
msgid "{0} — {1}"
msgstr "{0} — {1}"

#. Translators: Notification when clicking on "Copy Code to Clipboard" button
#: src/ui/action_view.rs:256
msgid "Copied Code to Clipboard"
msgstr "Code copié dans le presse-papiers"

#. Translators: Notification when clicking on "Copy Link to Clipboard" button
#: src/ui/action_view.rs:273
msgid "Copied Link to Clipboard"
msgstr "Lien copié dans le presse-papiers"

#: src/ui/action_view.rs:287
msgid "Copied Error to Clipboard"
msgstr "Erreur copiée dans le presse-papiers"

#: src/ui/action_view.rs:289
msgid "No error available"
msgstr "Aucune erreur disponible"

#: src/ui/action_view.rs:454
msgid "Creating Archive"
msgstr "Création d’une archive"

#: src/ui/action_view.rs:458
msgid "Compressing folder “{}”"
msgstr "Compression du dossier « {} »"

#. Translators: Title
#: src/ui/action_view.rs:474 src/ui/action_view.rs:537
msgid "Connecting"
msgstr "Connexion"

#. Translators: Description, Filename
#: src/ui/action_view.rs:477
msgid "Requesting file transfer"
msgstr "Demande de transfert de fichier"

#. Translators: Description, argument is filename
#: src/ui/action_view.rs:498
msgid "Ready to send “{}”."
msgstr "Prêt à envoyer « {} »."

#. Translators: Help dialog line 1, Code words and QR code visible,
#: src/ui/action_view.rs:503
msgid ""
"The receiver needs to enter or scan this code to begin the file transfer."
msgstr ""
"Le destinataire doit saisir ou scanner ce code pour commencer le transfert "
"de fichier."

#: src/ui/action_view.rs:507
msgid "The QR code is compatible with the following apps: {}."
msgstr "Le code QR est compatible avec les applications suivantes : {}."

#: src/ui/action_view.rs:515
msgid ""
"You have entered a custom rendezvous server URL in preferences. Please "
"verify the receiver also uses the same rendezvous server."
msgstr ""
"Vous avez saisi une URL personnalisée pour le serveur de mise en relation "
"dans les préférences. Veuillez vérifier que le destinataire utilise "
"également le même serveur de mise en relation."

#: src/ui/action_view.rs:520
msgid "Click the QR code to copy the link to the clipboard."
msgstr "Cliquez sur le code QR pour copier le lien dans le presse-papiers."

#. Translators: Description, Transfer Code
#: src/ui/action_view.rs:540
msgid "Connecting to peer with code “{}”"
msgstr "Connexion au pair avec le code « {} »"

#: src/ui/action_view.rs:550
msgid "Connected to Peer"
msgstr "Connecté au pair"

#. Translators: Description
#: src/ui/action_view.rs:559
msgid "Preparing to send file"
msgstr "Préparation de l’envoi du fichier"

#. Translators: Description
#: src/ui/action_view.rs:566
msgid "Preparing to receive file"
msgstr "Préparation de la réception du fichier"

#. Translators: File receive confirmation message dialog; Filename, File size
#: src/ui/action_view.rs:580
msgid ""
"Your peer wants to send you “{0}” (Size: {1}).\n"
"Do you want to download this file? The default action will save the file to "
"your Downloads folder."
msgstr ""
"Votre pair souhaite vous envoyer « {0} » (Taille : {1}).\n"
"Voulez-vous télécharger ce fichier ? L’action par défaut l’enregistrera dans "
"votre dossier Téléchargements."

#: src/ui/action_view.rs:585
msgid "Ready to Receive File"
msgstr "Prêt à recevoir le fichier"

#: src/ui/action_view.rs:587
msgid ""
"A file is ready to be transferred. The transfer needs to be acknowledged."
msgstr ""
"Un fichier est prêt à être transféré. Le transfert a besoin d’être approuvé."

#. Translators: Description, During transfer
#: src/ui/action_view.rs:617
msgid "File “{}” via local network direct transfer"
msgstr "Fichier « {} » par transfert direct via le réseau local"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:620
msgid "File “{}” via direct transfer"
msgstr "Fichier « {} » par transfert direct"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:626
msgid "File “{0}” via relay {1}"
msgstr "Fichier « {0} » par le relais {1}"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:629
msgid "File “{}” via relay"
msgstr "Fichier « {} » par relais"

#. Translators: Description, During transfer
#: src/ui/action_view.rs:633
msgid "File “{}” via Unknown connection method"
msgstr "Fichier « {} » par méthode de connexion inconnue"

#. Translators: Title
#: src/ui/action_view.rs:640
msgid "Sending File"
msgstr "Envoi du fichier"

#. Translators: Description, Filename
#: src/ui/action_view.rs:663
msgid "Successfully sent file “{}”"
msgstr "Fichier « {} » envoyé avec succès"

#. Translators: Filename
#: src/ui/action_view.rs:681
msgid "File has been saved to the selected folder as “{}”"
msgstr ""
"Le fichier a été enregistré dans le dossier sélectionné sous le nom « {} »"

#. Translators: Filename
#: src/ui/action_view.rs:688
msgid "File has been saved to the Downloads folder as “{}”"
msgstr ""
"Le fichier a été enregistré dans le dossier Téléchargements sous le nom "
"« {} »"

#. Translators: Title
#: src/ui/action_view.rs:714 src/ui/action_view.ui:292
msgid "File Transfer Failed"
msgstr "Échec du transfert de fichier"

#: src/ui/action_view.rs:716
msgid "The file transfer failed: {}"
msgstr "Le transfert de fichier a échoué : {}"

#. Translators: When opening a file
#: src/ui/action_view.rs:834
msgid "Specified file / directory does not exist"
msgstr "Le fichier ou le répertoire spécifié n’existe pas"

#: src/ui/action_view.rs:856
msgid ""
"Error parsing rendezvous server URL. An invalid URL was entered in the "
"settings."
msgstr ""
"Erreur lors de l’analyse de l’URL du serveur de mise en relation. Une URL "
"non valide a été saisie dans les paramètres."

#: src/ui/action_view.rs:863
msgid "Error parsing transit URL. An invalid URL was entered in the settings."
msgstr ""
"Erreur lors de l’analyse de l’URL de transit. Une URL non valide a été "
"saisie dans les paramètres."

#: src/ui/action_view.rs:959
msgid "Invalid path selected: {}"
msgstr "Chemin d’accès sélectionné non valide : {}"

#. Translators: Above progress bar for creating an archive to send as a folder
#: src/ui/action_view.rs:1204
msgid "{} File - Size: {}"
msgid_plural "{} Files - Size: {}"
msgstr[0] "{} fichier − Taille : {}"
msgstr[1] "{} fichiers − Taille : {}"

#: src/ui/action_view.ui:4
msgid "Save As"
msgstr "Enregistrer sous"

#. Translators: Button; Transmit Link is a noun
#: src/ui/action_view.ui:108
msgid "Copy Transmit Link"
msgstr "Copier le lien de transfert"

#: src/ui/action_view.ui:133
msgid "Your Transmit Code"
msgstr "Votre code de transfert"

#. Translators: Button; Transmit Code is a noun
#: src/ui/action_view.ui:181
msgid "Copy Transmit Code"
msgstr "Copier le code de transfert"

#. Translators: Title
#: src/ui/action_view.ui:198
msgid "Accept File Transfer?"
msgstr "Accepter le transfert de fichier ?"

#: src/ui/action_view.ui:208
msgid "Save to Downloads Folder"
msgstr "Enregistrer dans le dossier Téléchargements"

#. Translators: Button
#: src/ui/action_view.ui:210
msgid "_Accept"
msgstr "_Accepter"

#. Translators: Button
#: src/ui/action_view.ui:225
msgid "Sa_ve As…"
msgstr "Enregistrer _sous…"

#. Translators: Title
#: src/ui/action_view.ui:245
msgid "File Transfer Successful"
msgstr "Transfert du fichier réussi"

#. Translators: Button
#: src/ui/action_view.ui:255
msgid "_Open File"
msgstr "_Ouvrir le fichier"

#. Translators: Button
#: src/ui/action_view.ui:271 src/ui/window.ui:51
msgid "_Show in Folder"
msgstr "Afficher dans le _dossier"

#. Translators: Button
#: src/ui/action_view.ui:298
msgid "Co_py Error Message"
msgstr "_Copier le message d’erreur"

#. Translators: Button
#: src/ui/action_view.ui:316
msgid "_Cancel"
msgstr "A_nnuler"

#: src/ui/application.rs:60
msgid "Unable to use transfer link: another transfer already in progress"
msgstr ""
"Impossible d’utiliser le lien de transfert : un autre transfert est déjà en "
"cours"

#: src/ui/application.rs:207
msgid "Sending a File"
msgstr "Envoi d’un fichier"

#: src/ui/application.rs:208
msgid "Receiving a File"
msgstr "Réception d’un fichier"

#: src/ui/camera.rs:245
msgid ""
"Camera access denied. Open Settings and allow Warp to access the camera."
msgstr ""
"Accès à la caméra refusé. Ouvrez les paramètres et autorisez Warp à accéder "
"à la caméra."

#: src/ui/camera.rs:249
msgid "Failed to start the camera: {}"
msgstr "Échec de démarrage de la caméra : {}"

#: src/ui/camera.rs:252
msgid "Error"
msgstr "Erreur"

#: src/ui/camera.rs:341
msgid "Could not use the camera portal: {}"
msgstr "Impossible d’utiliser le portail de caméra : {}"

#: src/ui/camera.rs:348
msgid "Could not start the device provider: {}"
msgstr "Impossible de démarrer le fournisseur de périphérique : {}"

#: src/ui/camera.rs:379
msgid "No Camera Found"
msgstr "Aucune caméra trouvée"

#: src/ui/camera.rs:381
msgid "Connect a camera to scan QR codes"
msgstr "Connecter une caméra pour scanner des codes QR"

#: src/ui/camera.ui:11 src/ui/window.ui:216
msgid "Scan QR Code"
msgstr "Scanner un code QR"

#: src/ui/camera.ui:48
msgid "_Retry"
msgstr "_Réessayer"

#: src/ui/camera.ui:59
msgid "_Troubleshooting"
msgstr "_Dépannage"

#: src/ui/fs.rs:16
msgid "Downloads dir missing. Please set XDG_DOWNLOAD_DIR"
msgstr ""
"Répertoire de téléchargement manquant. Veuillez définir XDG_DOWNLOAD_DIR"

#: src/ui/licenses.rs:149
msgid "Licensed under the <a href=\"{}\">{}</a>."
msgstr "Sous licence <a href=\"{}\">{}</a>."

#. Translators: License information without a link to the software license
#: src/ui/licenses.rs:154
msgid "Licensed under the {}."
msgstr "Sous licence {}."

#: src/ui/preferences.rs:97
msgid ""
"Changing the rendezvous server URL needs to be done on both sides of the "
"transfer. Only enter a server URL you can trust.\n"
"\n"
"Leaving these entries empty will use the app defaults:\n"
"Rendezvous Server: “{0}”\n"
"Transit Server: “{1}”"
msgstr ""
"La modification de l’URL du serveur de mise en relation doit être effectuée "
"des deux côtés du transfert. Ne saisissez que des URL de serveurs auxquels "
"vous pouvez faire confiance.\n"
"\n"
"Les valeurs par défaut de l’application seront utilisées si ces entrées "
"restent vides :\n"
"Serveur de mise en relation : « {0} »\n"
"Serveur de transit : « {1} »"

#: src/ui/preferences.ui:4
msgid "Preferences"
msgstr "Préférences"

#: src/ui/preferences.ui:8
msgid "Network"
msgstr "Réseau"

#: src/ui/preferences.ui:12
msgid "Code Words"
msgstr "Mots du code"

#: src/ui/preferences.ui:13
msgid ""
"The code word count determines the security of the transfer.\n"
"\n"
"A short code is easy to remember but increases the risk of someone else "
"guessing the code. As a code may only be guessed once, the risk is very "
"small even with short codes. A length of 4 is very secure."
msgstr ""
"Le nombre de mots du code détermine la sécurité du transfert.\n"
"\n"
"Un code court est facile à retenir mais augmente le risque que quelqu’un "
"d’autre ne devine le code. Comme un code ne peut être deviné qu’une seule "
"fois, le risque est très faible, même avec des codes courts. Une longueur de "
"4 est très sûre."

#: src/ui/preferences.ui:18
msgid "Code Word Count"
msgstr "Nombre de mots du code"

#: src/ui/preferences.ui:26
msgid "Server URLs"
msgstr "URL du serveur"

#: src/ui/preferences.ui:29
msgid "Rendezvous Server URL"
msgstr "URL du serveur de mise en relation"

#: src/ui/preferences.ui:36
msgid "Transit Server URL"
msgstr "URL du serveur de transit"

#: src/ui/welcome_dialog.ui:10
msgid "Welcome"
msgstr "Bienvenue"

#: src/ui/welcome_dialog.ui:23
msgid "Welcome to Warp"
msgstr "Bienvenue dans Warp"

#. Translators: Button
#: src/ui/welcome_dialog.ui:31
msgid "Next"
msgstr "Suivant"

#: src/ui/welcome_dialog.ui:79
msgid "Introduction"
msgstr "Introduction"

#: src/ui/welcome_dialog.ui:90
msgid ""
"Warp makes file transfer simple. To get started, both parties need to "
"install Warp on their devices.\n"
"\n"
"After selecting a file to transmit, the sender needs to tell the receiver "
"the displayed transmit code. This is preferably done via a secure "
"communication channel.\n"
"\n"
"When the receiver has entered the code, the file transfer can begin.\n"
"\n"
"For more information about Warp, open the Help section from the Main Menu."
msgstr ""
"Warp rend le transfert de fichiers simple. Pour commencer, les deux parties "
"doivent installer Warp sur leur appareil.\n"
"\n"
"Après avoir sélectionné le fichier à transférer, l’expéditeur doit "
"communiquer au destinataire le code de transfert affiché ; de préférence via "
"un canal de communication sécurisé.\n"
"\n"
"Quand le destinataire a saisi le code, le transfert de fichier peut "
"commencer.\n"
"\n"
"Pour plus d’informations sur Warp, ouvrez la rubrique Aide depuis le menu "
"principal."

#. Translators: Big button to finish welcome screen
#: src/ui/welcome_dialog.ui:103
msgid "Get Started Using Warp"
msgstr "Commencer à utiliser Warp"

#: src/ui/window.rs:135
msgid ""
"Error loading config file “{0}”, using default config.\n"
"Error: {1}"
msgstr ""
"Erreur lors du chargement du fichier de configuration « {0} », en utilisant "
"la configuration par défaut.\n"
"Erreur : {1}"

#: src/ui/window.rs:284
msgid "Error saving configuration file: {}"
msgstr "Erreur lors de l’enregistrement du fichier de configuration : {}"

#: src/ui/window.rs:304
msgid "Tobias Bernard"
msgstr "Tobias Bernard"

#: src/ui/window.rs:304
msgid "Sophie Herold"
msgstr "Sophie Herold"

#: src/ui/window.rs:305
msgid "translator-credits"
msgstr ""
"Irénée Thirion <irenee.thirion@e.email>\n"
"Charles Monzat <charles.monzat@free.fr>"

#: src/ui/window.rs:370
msgid "Select File to Send"
msgstr "Sélectionnez le fichier à envoyer"

#: src/ui/window.rs:378
msgid "Select Folder to Send"
msgstr "Sélectionnez le dossier à envoyer"

#: src/ui/window.rs:435
msgid "“{}” appears to be an invalid Transmit Code. Please try again."
msgstr ""
"« {} » semble être un code de transfert non valide. Veuillez réessayer."

#: src/ui/window.rs:556
msgid "Sending files with a preconfigured code is not yet supported"
msgstr ""
"L’envoi de fichiers avec un code préconfiguré n’est pas encore pris en charge"

#. Translators: menu item
#: src/ui/window.ui:7
msgid "_Preferences"
msgstr "_Préférences"

#. Translators: menu item
#: src/ui/window.ui:12
msgid "_Keyboard Shortcuts"
msgstr "Raccourcis _clavier"

#. Translators: menu item
#: src/ui/window.ui:17
msgid "_Help"
msgstr "_Aide"

#. Translators: menu item
#: src/ui/window.ui:22
msgid "_About Warp"
msgstr "À _propos de Warp"

#. Translators: Notification when code was automatically detected in clipboard and inserted into code entry on receive page
#: src/ui/window.ui:33
msgid "Inserted code from clipboard"
msgstr "Code inséré depuis le presse-papiers"

#. Translators: File receive confirmation message dialog title
#: src/ui/window.ui:38
msgid "Abort File Transfer?"
msgstr "Abandonner le transfert de fichier ?"

#: src/ui/window.ui:39
msgid "Do you want to abort the current file transfer?"
msgstr "Voulez-vous abandonner le transfert de fichier actuel ?"

#: src/ui/window.ui:42
msgid "_Continue"
msgstr "_Continuer"

#: src/ui/window.ui:43
msgid "_Abort"
msgstr "_Abandonner"

#. Translators: Error dialog title
#: src/ui/window.ui:48
msgid "Unable to Open File"
msgstr "Impossible d’ouvrir le fichier"

#: src/ui/window.ui:50 src/util/error.rs:198
msgid "_Close"
msgstr "_Fermer"

#: src/ui/window.ui:98
msgid "Main Menu"
msgstr "Menu principal"

#: src/ui/window.ui:117
msgid "_Send"
msgstr "En_voyer"

#: src/ui/window.ui:129
msgid "Send File"
msgstr "Envoyer un fichier"

#: src/ui/window.ui:130
msgid "Select or drop the file or directory to send"
msgstr "Sélectionnez ou déposez le fichier ou répertoire à envoyer"

#. Translators: Button
#: src/ui/window.ui:142
msgid "Select _File…"
msgstr "Sélectionner un _fichier…"

#. Translators: Button
#: src/ui/window.ui:157
msgid "Select F_older…"
msgstr "Sélectionner un _dossier…"

#: src/ui/window.ui:177
msgid "_Receive"
msgstr "_Recevoir"

#: src/ui/window.ui:186
msgid "Receive File"
msgstr "Recevoir un fichier"

#. Translators: Text above code input box, transmit code is a noun
#: src/ui/window.ui:188
msgid "Enter the transmit code from the sender"
msgstr "Saisissez le code de transfert de l’expéditeur"

#. Translators: Button
#: src/ui/window.ui:237
msgid "Receive _File"
msgstr "Recevoir un f_ichier"

#: src/ui/window.ui:263
msgid "File Transfer"
msgstr "Transfert de fichier"

#: src/util.rs:36 src/util.rs:42
msgid "Failed to open downloads folder."
msgstr "Échec de l’ouverture du dossier Téléchargements."

#: src/util.rs:209 src/util.rs:282
msgid "The URI format is invalid"
msgstr "Le format de l’URI n’est pas valide"

#: src/util.rs:213 src/util.rs:217
msgid "The code does not match the required format"
msgstr "Le code ne correspond pas au format requis"

#: src/util.rs:232 src/util.rs:238
msgid "Unknown URI version: {}"
msgstr "Version d’URI inconnue : {}"

#: src/util.rs:247
msgid "The URI parameter “rendezvous” contains an invalid URL: “{}”"
msgstr ""
"Le paramètre d’URI « serveur de mise en relation » contient une URL non "
"valide : « {} »"

#: src/util.rs:259
msgid "The URI parameter “role” must be “follower” or “leader” (was: “{}”)"
msgstr ""
"Le paramètre d’URI « role » doit être « follower » ou « leader » (était "
"auparavant : « {} »)"

#: src/util.rs:266
msgid "Unknown URI parameter “{}”"
msgstr "Paramètre d’URI « {} » inconnu"

#: src/util/error.rs:181
msgid "An error occurred"
msgstr "Une erreur s’est produite"

#: src/util/error.rs:209 src/util/error.rs:293
msgid "Corrupt or unexpected message received"
msgstr "Corruption ou réception d’un message inattendu"

#: src/util/error.rs:214
msgid ""
"The rendezvous server will not allow further connections for this code. A "
"new code needs to be generated."
msgstr ""
"Le serveur de mise en relation n’autorisera plus de connexions pour ce code. "
"Un nouveau code doit être généré."

#: src/util/error.rs:216
msgid ""
"The rendezvous server removed the code due to inactivity. A new code needs "
"to be generated."
msgstr ""
"Le serveur de mise en relation a supprimé le code pour cause d’inactivité. "
"Un nouveau code doit être généré."

#: src/util/error.rs:218
msgid "The rendezvous server responded with an unknown message: {}"
msgstr "Le serveur de mise en relation a répondu avec un message inconnu : {}"

#: src/util/error.rs:221
msgid ""
"Error connecting to the rendezvous server.\n"
"You have entered a custom rendezvous server URL in preferences. Please "
"verify the URL is correct and the server is working."
msgstr ""
"Erreur de connexion au serveur de mise en relation.\n"
"Vous avez saisi une URL personnalisée pour le serveur de mise en relation "
"dans les préférences. Veuillez vérifier que l’URL est correcte et que le "
"serveur fonctionne."

#: src/util/error.rs:223
msgid ""
"Error connecting to the rendezvous server.\n"
"Please try again later / verify you are connected to the internet."
msgstr ""
"Erreur lors de la connexion au serveur de mise en relation.\n"
"Veuillez réessayer plus tard ou vérifiez que vous êtes connecté à Internet."

#: src/util/error.rs:227
msgid ""
"Encryption key confirmation failed. If you or your peer didn't mistype the "
"code, this is a sign of an attacker guessing passwords. Please try again "
"some time later."
msgstr ""
"La confirmation de la clé de chiffrement a échoué. Si vous ou votre pair "
"n’avez pas fait d’erreur en saisissant le code, c’est le signe qu’une "
"personne malveillante essaye de deviner les mots de passe. Veuillez "
"réessayer un peu plus tard."

#: src/util/error.rs:229
msgid "Cannot decrypt a received message"
msgstr "Impossible de déchiffrer un message reçu"

#: src/util/error.rs:230 src/util/error.rs:304 src/util/error.rs:310
msgid "An unknown error occurred"
msgstr "Une erreur inconnue s’est produite"

#: src/util/error.rs:236
msgid "File / Directory not found"
msgstr "Fichier / répertoire introuvable"

#: src/util/error.rs:237
msgid "Permission denied"
msgstr "Permission refusée"

#: src/util/error.rs:245
msgid "Canceled"
msgstr "Annulé"

#: src/util/error.rs:247
msgid "Portal service response: Permission denied"
msgstr "Réponse du service de portail : permission refusée"

#: src/util/error.rs:250
msgid "The portal service has failed: {}"
msgstr "Le service de portail a échoué : {}"

#: src/util/error.rs:253
msgid "Error communicating with the portal service via zbus: {}"
msgstr "Erreur de communication avec le service de portail par zbus : {}"

#: src/util/error.rs:256 src/util/error.rs:257
msgid "Portal error: {}"
msgstr "Erreur de portail : {}"

#: src/util/error.rs:268
msgid "Transfer was not acknowledged by peer"
msgstr "Le transfert n’a pas été autorisé par le pair"

#: src/util/error.rs:270
msgid "The received file is corrupted"
msgstr "Le fichier reçu est corrompu"

#: src/util/error.rs:276
msgid ""
"The file contained a different amount of bytes than advertised! Sent {} "
"bytes, but should have been {}"
msgstr ""
"Le fichier contenait une quantité d’octets différente de celle annoncée ! {} "
"octets envoyés, mais cela aurait du être {}"

#: src/util/error.rs:281
msgid "The other side has cancelled the transfer"
msgstr "L’autre partie a annulé le transfert"

#: src/util/error.rs:283
msgid "The other side has rejected the transfer"
msgstr "L’autre partie a rejeté le transfert"

#: src/util/error.rs:285
msgid "Something went wrong on the other side: {}"
msgstr "Quelque chose s’est mal passé de l’autre côté : {}"

#: src/util/error.rs:300
msgid "Error while establishing file transfer connection"
msgstr ""
"Erreur lors de l’établissement de la connexion pour le transfert de fichier"

#: src/util/error.rs:302
msgid "Unknown file transfer error"
msgstr "Erreur de transfert du fichier inconnue"

#: src/util/error.rs:311
msgid "An unknown error occurred while creating a zip file: {}"
msgstr ""
"Une erreur inconnue s’est produite lors de la création du fichier zip : {}"

#: src/util/error.rs:312
msgid ""
"An unexpected error occurred. Please report an issue with the error message."
msgstr ""
"Une erreur inattendue s’est produite. Veuillez signaler le problème en "
"indiquant le message d’erreur."

#~ msgid "Error talking to the camera portal"
#~ msgstr "Erreur de communication avec le portail de caméra"

#~ msgid "Portal Error: {}"
#~ msgstr "Erreur de portail : {}"
